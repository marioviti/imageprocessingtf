from core.convolutions import convolve
from core.data import to_tensor
import tensorflow as tf
import numpy as np
import sys

def gaussian_blur(rank,sigma=1):
    x_sigma = np.arange(-1,2,1/sigma)
    g_sigma = np.exp(-0.5*(x_sigma**2))
    g_sigma = g_sigma/g_sigma.sum()
    if rank == 1:
        return np.expand_dims(g_sigma, -1)
    if rank == 2:
        return np.expand_dims( np.outer(g_sigma,g_sigma), -1)

def sobelpervitgauss(rank):
    if rank == 2:
        k = np.outer([[1,0,-1],[0.25,0.5,0.25]],
                     [[-1,0,1],[0.25,0.5,0.25]])
        k = np.stack((k[0:3,0:3], k[0:3,3:], k[3:,0:3], k[3:,3:])).transpose(1,2,0)
    return k

def sobel(rank):
    if rank == 2:
        dx = np.expand_dims(np.outer([0.25,0.5,0.25], [-1,0,1]),-1)
        dy = np.expand_dims(np.outer([-1,0,1], [0.25,0.5,0.25]),-1)
        k = dx,dy
    return k

def sobel_filter(img,rank=2):
    img_t = to_tensor(img)
    if rank==2:
        dx,dy = sobel(rank)
        dx,dy = to_tensor(dx), to_tensor(dy) 
        imgx = convolve(img_t,dx,rank=rank)
        imgy = convolve(img_t,dy,rank=rank)
        output = imgx, imgy
    return output

def sobelpervitgauss_filter(img,rank=2):
    img_t = to_tensor(img)
    img_t = tf.tile(img_t,[1,1,4])
    if rank==2:
        kernel = sobelpervitgauss(rank)
        kernel_t = to_tensor(kernel)
        img_filtered_t = convolve(img_t,kernel_t,rank=rank)
        output = img_filtered_t
    return output

def gaussian_blur_filter(img,sigma=1,rank=2):
    img_t = to_tensor(img)
    if rank==2:
        gsigma = gaussian_blur(rank,sigma=sigma)
        gsigma_t = to_tensor(gsigma)
        img_sigma_t = convolve(img_t,gsigma_t,rank=rank)
        output = img_sigma_t
    return output
    