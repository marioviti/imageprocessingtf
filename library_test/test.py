from configuration import LENA_DIR
from core.io import load_img
from edge_detection import sobel_filter, sobelpervitgauss_filter, gaussian_blur_filter
from matplotlib import pyplot as plt
import numpy as np
import tensorflow as tf

def sobel_test():
    img = load_img(LENA_DIR)[:,:,0:1]
    dx,dy = sobel_filter(img)

    with tf.Session() as sess:
        img_dx = sess.run(dx)
        img_dy = sess.run(dy)
    
    f, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True)
    ax1.imshow(img[...,0])
    ax1.set_title('image')
    ax2.imshow(img_dx[0,:,:,0].astype(np.uint8))
    ax2.set_title('dx')
    ax3.imshow(img_dy[0,:,:,0].astype(np.uint8))
    ax3.set_title('dy')
    plt.show()


def sobelpervitgauss_test():
    img = load_img(LENA_DIR)[:,:,0:1]
    img_filtered_t = sobelpervitgauss_filter(img)
    
    with tf.Session() as sess:
        img_filtered = sess.run(img_filtered_t)
    
    f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
    ax1.imshow(img[...,0])
    ax1.set_title('image')
    ax2.imshow(img_filtered[0,:,:,0].astype(np.uint8))
    ax2.set_title('sobelpervitgauss')
    plt.show()

def gaussian_blur_filter_test():
    img = load_img(LENA_DIR)[:,:,0:1]
    img_filtered_t = gaussian_blur_filter(img)
    
    with tf.Session() as sess:
        img_filtered = sess.run(img_filtered_t)
    
    f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
    ax1.imshow(img[...,0])
    ax1.set_title('image')
    ax2.imshow(img_filtered[0,:,:,0].astype(np.uint8))
    ax2.set_title('gaussian_blur_filter')
    plt.show()