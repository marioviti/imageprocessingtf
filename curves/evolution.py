"""
Curve evolution in time.
C:R->R^n

Base:
Velocity V deformation.

Property: 
1. tangent component do not affect the shape of the curve.
2. deformation is proportional to the curvature.
3. curvature is equal to the second derivative of curve.pyt
"""