import tensorflow as tf
import numpy as np
from core.data import to_tensor

def convolve(x,y,rank=2,padding='same'):
    """
    Wrapping tensorflow 2D and 3D convolution.
    PARAMS:
        x (tf.tensor) input h,w,c.
        y (tf.tensor) kernel shaped h,w,c.
    RETURNS:
        x*y (tf.tensor) .
    """
    x = tf.expand_dims(x,0)
    y = tf.expand_dims(y,-1)
    if rank == 2:
        x_conv_y = tf.nn.conv2d(x,y,strides=[1,1,1,1],padding=padding.upper(),data_format="NHWC")
    return x_conv_y


