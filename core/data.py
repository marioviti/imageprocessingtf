import tensorflow as tf

def to_tensor(arg):
  arg_t = tf.convert_to_tensor(arg, dtype='float16')
  return arg_t