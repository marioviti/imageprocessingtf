from PIL import Image
import numpy as np

def load_img(path_to_file):
    with Image.open(path_to_file) as image:         
        im_arr = np.fromstring(image.tobytes(), dtype=np.uint8)
        channels = im_arr.shape[0]//(image.size[0]*image.size[1])
        im_arr = im_arr.reshape(*image.size,channels)                                   
    return im_arr
